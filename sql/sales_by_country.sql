SELECT
    "BillingCountry" AS "Country",
    SUM("Total") AS "Sales",
    COUNT(*) AS "NumInvoices"
FROM
    "Invoice"
GROUP BY
    "BillingCountry"
