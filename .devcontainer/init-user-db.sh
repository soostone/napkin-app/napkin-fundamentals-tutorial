#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
  CREATE SCHEMA chinook;
  CREATE SCHEMA development;
  CREATE SCHEMA production;
EOSQL

PGOPTIONS='--search_path=chinook' PGCLIENTENCODING=iso-8859-1 psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" -1 -q < /chinook.sql
