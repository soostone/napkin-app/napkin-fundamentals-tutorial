SELECT
    top_10_countries."Country" AS "Country",
    COUNT(*) AS "NumCustomers"
FROM
    top_10_countries
    JOIN "Customer" ON top_10_countries."Country" = "Customer"."Country"
GROUP BY top_10_countries."Country"
