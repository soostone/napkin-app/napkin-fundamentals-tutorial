#!/bin/bash

mkdir -p dataset

if [ ! -f dataset/Chinook_Sqlite.sqlite ]; then
  curl -f -s -L -o dataset/Chinook_Sqlite.sqlite https://github.com/lerocha/chinook-database/raw/master/ChinookDatabase/DataSources/Chinook_Sqlite.sqlite
  echo "Downloaded Sqlite data set"
fi


if [ -f chinook.db ]; then
  echo "Sqlite DB already exist"
else
  cp dataset/Chinook_Sqlite.sqlite chinook.db
  echo "Chinook database has been loaded to chinook.db Sqlite file"
fi
