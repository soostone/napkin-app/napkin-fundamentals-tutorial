chinook-analytics spec
---

A napkin example project used in [napkin's fundamental tutorial](https://docs.napkin.run/fundamentals)

## Getting started

Start by cloning the project

```sh
git clone
```

*Note: vs-code users may opt to clone the project form vs-code, [Setting up a repository](https://code.visualstudio.com/docs/editor/github#_setting-up-a-repository).*
### Visual Studio Code users

For [vscode](https://code.visualstudio.com/) users, first open your project as outlined in [vs-code quick start guide](https://code.visualstudio.com/docs/remote/containers#_quick-start-open-an-existing-folder-in-a-container).


Then open your [vs-code integrated terminal](https://code.visualstudio.com/docs/editor/integrated-terminal):

```sh
$ ls -l
```

```sh
drwxrwxr-x 9 napkin napkin    4096 Jan 14 18:03 ./
drwxr-xr-x 1 root   root      4096 Jan 14 17:32 ../
-rw-r--r-- 1 napkin napkin 1079296 Jan 14 18:03 chinook.db
drwxr-xr-x 2 napkin napkin    4096 Jan 14 18:03 data/
drwxr-xr-x 2 napkin napkin    4096 Jan 14 17:32 dataset/
drwxrwxr-x 2 napkin napkin    4096 Jan 14 16:53 .devcontainer/
drwxrwxr-x 8 napkin napkin    4096 Jan 14 16:53 .git/
-rw-rw-r-- 1 napkin napkin      98 Jan 14 16:53 .gitignore
-rw-rw-r-- 1 napkin napkin     688 Jan 14 16:41 .gitlab-ci.yml
-rw-rw-r-- 1 napkin napkin      71 Jan 14 16:52 hie.yaml
-rw-rw-r-- 1 napkin napkin   28046 Jan 14 17:32 napkin-schema.generated.json
-rw-rw-r-- 1 napkin napkin    2029 Jan 14 16:52 README.md
drwxrwxr-x 2 napkin napkin    4096 Jan 14 16:52 specs/
drwxrwxr-x 2 napkin napkin    4096 Jan 14 16:52 sql/
drwxrwxr-x 2 napkin napkin    4096 Jan 14 16:53 .vscode/
```

Next verify PostgreSQL and SQLite Chinook installation:

```sh
$ echo ' \dt chinook.*' | psql
```

```sh
             List of relations
 Schema  |     Name      | Type  |  Owner
---------+---------------+-------+----------
 chinook | Album         | table | postgres
 chinook | Artist        | table | postgres
 chinook | Customer      | table | postgres
 chinook | Employee      | table | postgres
 chinook | Genre         | table | postgres
 chinook | Invoice       | table | postgres
 chinook | InvoiceLine   | table | postgres
 chinook | MediaType     | table | postgres
 chinook | Playlist      | table | postgres
 chinook | PlaylistTrack | table | postgres
 chinook | Track         | table | postgres
(11 rows)
```


```sh
$ sqlite3 ./chinook.db ".tables"
```

``` sh
Album                       Playlist
Artist                      PlaylistTrack
Customer                    Track
Employee                    customers_total_purchase
Genre                       sales_by_country
Invoice                     top_10_countries
InvoiceLine                 top_10_countries_customers
MediaType                   top_20_customers
```

And finally execute the pipeline:

``` sh
napkin validate --spec-file specs/spec-sqlite.yaml
napkin validate --spec-file specs/spec-postgres.yaml

napkin run --spec-file specs/spec-sqlite.yaml
napkin run --spec-file specs/spec-postgres.yaml
```
