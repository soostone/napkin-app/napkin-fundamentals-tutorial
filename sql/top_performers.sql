SELECT
    *
FROM
    "{{{table}}}"
ORDER BY
    "{{{top_by}}}" DESC
FETCH FIRST {{{max_limit}}} ROWS ONLY