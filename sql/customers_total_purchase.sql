SELECT
    "Customer"."CustomerId" AS "CustomerId",
    "Customer"."Email" AS "Email",
    "Customer"."FirstName" AS "FirstName",
    "Customer"."LastName" AS "LastName",
    SUM("Invoice"."Total") AS "CustomerSales"
FROM
    "Customer"
    JOIN "Invoice" ON "Customer"."CustomerId" = "Invoice"."CustomerId"
GROUP BY
    "Customer"."CustomerId"